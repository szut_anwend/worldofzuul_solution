package de.szut.zuul.gamecontrol;

import de.szut.zuul.commands.*;
import de.szut.zuul.model.*;

/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game 
{
    private Parser parser;
    private Player player;
        
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        this.player= new Player();
        parser = new Parser();
        createRooms();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        Room marketsquare, templePyramid, tavern, sacrificialSite, hut, jungle, secretPassage, cave, beach, wizard, cellar;
      
        // create the rooms
        marketsquare = new Room("on the market square");
        templePyramid = new Room("in a temple pyramid");
        tavern = new Room("in the tavern at the market square");
        sacrificialSite = new Room("at a sacrificial site");
        hut = new Room("in a hut");
        jungle = new Room("in the jungle");
        secretPassage = new Room("in a secret passage");
        cave = new Room("in a cave");
        beach = new Room("on the beach");
        wizard = new Room("in the room of the wizard");
        cellar = new Room("in the cellar");

        // initialise room exits
        marketsquare.setExit("north", tavern);
        marketsquare.setExit("east", templePyramid);
        marketsquare.setExit("west", sacrificialSite);
        templePyramid.setExit("north", hut);
        templePyramid.setExit("west", marketsquare);
        templePyramid.setExit("up", wizard);
        templePyramid.setExit("down", cellar);
        tavern.setExit("south", marketsquare);
        tavern.setExit("east", hut);
        sacrificialSite.setExit("east", marketsquare);
        sacrificialSite.setExit("down", cave);
        hut.setExit("south", templePyramid);
        hut.setExit("east", jungle);
        hut.setExit("west", tavern);
        jungle.setExit("west", hut);
        secretPassage.setExit("east", cave);
        secretPassage.setExit("west", cave);
        cave.setExit("south", beach);
        cave.setExit("east", secretPassage);
        cave.setExit("up", sacrificialSite);
        beach.setExit("north", cave);
        wizard.setExit("down", templePyramid);
        cellar.setExit("west", secretPassage);
        cellar.setExit("up", templePyramid);

        //put items in rooms
        marketsquare.putItem(new Item("bow", "a bow made of wood", 0.5));
        cave.putItem(new Item("treasure", "a little treasure chest with coins", 7.5));
        wizard.putItem(new Item("arrows","a lot of arrows in a quiver", 1.0));
        jungle.putItem(new Herb("herb", "a medicine herb", 0.5));
        jungle.putItem(new Item("cocoa", "a little cocoa tree", 5));
        sacrificialSite.putItem(new Item("knife", "a gig, sharp knife", 1));
        hut.putItem(new Item("spear", "a spear with slingshot", 5.0));
        tavern.putItem(new Item("food", "a plate of hearty meat and corn porridge", 0.5));
        cellar.putItem(new Item("jewellery", "a very pretty headdress", 1));
        marketsquare.putItem(new Muffin("muffin", "a magic muffin",0.3, 5));

        player.goTo(marketsquare);  // start game on marketsquare

        setCommands();
    }

    private void setCommands() {
        this.parser.addCommand(new Help(this.parser, "through the jungle. At once there is a glade. On it there a buildings..."));
        this.parser.addCommand(new Go(this.player));
        this.parser.addCommand(new Quit());
        this.parser.addCommand(new Look(this.player));
        this.parser.addCommand(new Take(this.player));
        this.parser.addCommand(new Drop(this.player));
        this.parser.addCommand(new Eat(this.player));
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        while (true) {
            this.parser.readAndExecuteCommand();
        }
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("Welcome to the World of Zuul!");
        System.out.println("World of Zuul is a new, incredibly boring adventure game.");
        System.out.println("Type 'help' if you need help.");
        System.out.println();
        printRoomInformation();
    }

    private void printRoomInformation(){
        System.out.println(this.player.getCurrentRoom().getLongDescription());
        System.out.println();
    }
}
