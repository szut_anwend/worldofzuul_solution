package de.szut.zuul.gamecontrol;

import de.szut.zuul.commands.Command;
import de.szut.zuul.exceptions.NoValidCommandException;

import java.util.HashMap;

/**
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 * 
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class CommandWords
{
    // a constant array that holds all valid command words
    private HashMap<String, Command> commands;

    /**
     * Constructor - initialise the command words.
     */
    public CommandWords()
    {
        this.commands = new HashMap<>();
    }

    public String showAll()
    {
        StringBuilder commands = new StringBuilder();
        for(String command : this.commands.keySet())
            commands.append(command + " ");
        return commands.toString();
    }

    public void addCommand(Command cmd){
        this.commands.put(cmd.getCommandWord(), cmd);
    }

    public Command getCommand(String commandWord) throws NoValidCommandException {
        Command cmd = this.commands.get(commandWord);
        if(cmd != null)
            return cmd;
        throw new NoValidCommandException("No valid command!");
    }
}
