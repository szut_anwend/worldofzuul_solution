package de.szut.zuul.gamecontrol;

import de.szut.zuul.commands.Command;
import de.szut.zuul.exceptions.NoValidCommandException;

import java.util.Scanner;

/**
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 * 
 * This parser reads user input and tries to interpret it as an "Adventure"
 * command. Every time it is called it reads a line from the terminal and
 * tries to interpret the line as a two word command. It returns the command
 * as an object of class Command.
 *
 * The parser has a set of known command words. It checks user input against
 * the known commands, and if the input is not one of the known commands, it
 * returns a command object that is marked as an unknown command.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */
public class Parser 
{
    private CommandWords commands;  // holds all valid command words
    private Scanner reader;         // source of command input

    /**
     * Create a parser to read from the terminal window.
     */
    public Parser() 
    {
        commands = new CommandWords();
        reader = new Scanner(System.in);
    }

    public void readAndExecuteCommand(){
        String line = readLine();
        String[] result = parseLine(line);
        executeCommand(result[0], result[1]);
    }

    private String readLine() {
        System.out.print("> ");
        String line = this.reader.nextLine();
        return line;
    }

    private String[] parseLine(String line) {
        Scanner tokenizer = new Scanner(line);
        String[] result = new String[2];
        if(tokenizer.hasNext()) {
            result[0] = tokenizer.next();
            if (tokenizer.hasNext()) {
                result[1] = tokenizer.next();
            }
        }
        tokenizer.close();
        return result;
    }

    private void executeCommand(String wort1, String wort2) {
        try{
            Command cmd = this.commands.getCommand(wort1);
            cmd.execute(wort2);
        }
        catch(NoValidCommandException e){
            System.out.println(e.getMessage());
        }
    }

    public void addCommand(Command cmd){
        this.commands.addCommand(cmd);
    }

    public String showCommands(){
        return this.commands.showAll();
    }
}
