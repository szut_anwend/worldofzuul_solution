package de.szut.zuul.model;

import de.szut.zuul.exceptions.ItemNotFoundException;
import de.szut.zuul.exceptions.ItemTooHeavyException;
import de.szut.zuul.states.State;
import de.szut.zuul.states.Wounded;

import java.util.LinkedList;

public class Player {
    private Room currentRoom;
    private double loadCapacity;
    private LinkedList<Item> items;
    private State currentState;

    public Player(){
        this.loadCapacity=10.0;
        this.items = new LinkedList<Item>();
        this.currentState = Wounded.getInstance();
    }

    public void goTo(Room newRoom){
        this.currentRoom = newRoom;
    }

    public Room getCurrentRoom(){
        return this.currentRoom;
    }

    public void takeItem(Item item) throws ItemTooHeavyException {
        if(isTakePossible(item)){
            this.items.add(item);
        }
        else{
            throw new ItemTooHeavyException("Item is too heavy!");
        }
    }

    private boolean isTakePossible(Item item){
        return calculateWeight() + item.getWeight() <= this.loadCapacity;
    }

    private double calculateWeight() {
        double totalWeight = 0;
        for(Item item: this.items)
            totalWeight = totalWeight + item.getWeight();
        return totalWeight;
    }

    public Item dropItem(String name) throws ItemNotFoundException {
        for(Item item: this.items){
            if(name.equals(item.getName())){
                this.items.remove(item);
                return item;
            }
        }
        throw new ItemNotFoundException("You don‘t own this item!");
    }

    public String showStatus(){
        StringBuilder result = new StringBuilder(">status of the player\nloadcapacity:"+ this.loadCapacity+"\ntaken items: ");
        if(this.items.isEmpty())
        {
            result.append("none");
        }
        else
        {
            for(Item item: this.items)
                result.append(item.toString()+"; ");
            result.append("\nabsorbed weight: "+calculateWeight());
        }
        result.append("\ncurrent state: ").append(this.currentState).append("\n");
        return result.toString();
    }

    public void increaseLoadingCapacity(double value){
        this.loadCapacity += value;
    }

    public void setCurrentState(State state){
        this.currentState = state;
    }

    public void heal(){
        this.currentState=this.currentState.heal();
    }

    public void injureEasily(){
        this.currentState=this.currentState.injureEasily();
    }

    public void injureHeavy(){
        this.currentState=this.currentState.injureHeavy();
    }
}
