package de.szut.zuul.model;

public class Item {
    private String name;
    private String description;
    private double weight;

    public Item(String name, String description, double weight) {
        this.name = name;
        this.description = description;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return description + ", "+ weight + "kg" + " [" + name + "]";
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }
}
