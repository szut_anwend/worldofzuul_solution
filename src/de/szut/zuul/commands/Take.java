package de.szut.zuul.commands;

import de.szut.zuul.exceptions.ItemNotFoundException;
import de.szut.zuul.exceptions.ItemTooHeavyException;
import de.szut.zuul.model.Item;
import de.szut.zuul.model.Player;

public class Take implements Command{
    private final String COMMANDWORD = "take";
    private Player player;

    public Take(Player player){
        this.player = player;
    }

    @Override
    public void execute(String word) {
        takeItem(word);
        System.out.println(this.player.showStatus());
        System.out.println(this.player.getCurrentRoom().getLongDescription());
    }

    private void takeItem(String word){
        if(word == null){
            System.out.println("Which item do you want to take?");
            return;
        }
        Item item = null;
        try{
            item = this.player.getCurrentRoom().removeItem(word);
            this.player.takeItem(item);
        }
        catch(ItemNotFoundException e){
            System.out.println(e.getMessage());
        }
        catch(ItemTooHeavyException e){
            this.player.getCurrentRoom().putItem(item);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }
}
