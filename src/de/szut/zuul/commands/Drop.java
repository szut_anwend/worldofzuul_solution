package de.szut.zuul.commands;

import de.szut.zuul.exceptions.ItemNotFoundException;
import de.szut.zuul.model.Player;

public class Drop implements Command{
    private final String COMMANDWORD = "drop";
    private Player player;

    public Drop(Player player){
        this.player = player;
    }

    @Override
    public void execute(String word) {
        if(word==null) {
            System.out.println("Wich item do you want to drop?");
            return;
        }
        try{
            player.getCurrentRoom().putItem(this.player.dropItem(word));
            System.out.println(this.player.showStatus());
            System.out.println(this.player.getCurrentRoom().getLongDescription());
        }
        catch(ItemNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }
}