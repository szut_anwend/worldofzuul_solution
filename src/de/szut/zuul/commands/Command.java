package de.szut.zuul.commands;

public interface Command {
    void execute(String word);
    String getCommandWord();
}
