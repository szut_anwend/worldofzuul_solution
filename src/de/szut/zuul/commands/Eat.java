package de.szut.zuul.commands;

import de.szut.zuul.exceptions.ItemNotFoundException;
import de.szut.zuul.model.Muffin;
import de.szut.zuul.model.Player;

public class Eat implements Command{
    private final String COMMANDWORD = "eat";
    private Player player;

    public Eat(Player player){
        this.player = player;
    }

    @Override
    public void execute(String wort) {
        eat(wort);
        System.out.println(this.player.showStatus());
        System.out.println(this.player.getCurrentRoom().getLongDescription());
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }

    private void eat(String word)
    {
        if(word == null) {
            System.out.println("Wich item do you want to eat?");
            return;
        }

        if(word.equals("muffin"))
        {
            try{
                Muffin m = (Muffin) this.player.getCurrentRoom().removeItem("muffin");
                this.player.increaseLoadingCapacity(m.getLoadingCapacity());
            }
            catch(ItemNotFoundException e){
                System.out.println(e.getMessage());
            }
        }
        else if(word.equals("herb")) {
            try {
                this.player.getCurrentRoom().removeItem("herb");
                this.player.heal();
            } catch (ItemNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
