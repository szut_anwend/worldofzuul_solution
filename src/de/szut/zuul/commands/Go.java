package de.szut.zuul.commands;

import de.szut.zuul.exceptions.ItemNotFoundException;
import de.szut.zuul.model.Player;
import de.szut.zuul.model.Room;

public class Go implements Command{
    private final String COMMANDWORD = "go";
    private Player player;

    public Go(Player player){
        this.player = player;
    }

    @Override
    public void execute(String word) {
        if(word==null) {
            System.out.println("Go where?");
            return;
        }

        Room nextRoom = this.player.getCurrentRoom().getExit(word);
        if (nextRoom == null) {
            System.out.println("There is no door!");
        }
        else {
            player.goTo(nextRoom);
            printRoomInformation();
        }
    }

    private void printRoomInformation() {
        System.out.println();
        System.out.println(this.player.getCurrentRoom().getLongDescription());
        System.out.println();
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }
}