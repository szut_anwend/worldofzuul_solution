package de.szut.zuul.commands;

import de.szut.zuul.gamecontrol.Parser;

public class Help implements Command {
    private final String COMMANDWORD = "help";
    private Parser parser;
    private String mapHelptext;

    public Help(Parser parser, String text){
        this.parser = parser;
        this.mapHelptext=text;
    }

    @Override
    public void execute(String word) {
        System.out.println("You are lost. You are alone. You wander");
        System.out.println(this.mapHelptext);
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println(this.parser.showCommands());
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }
}
