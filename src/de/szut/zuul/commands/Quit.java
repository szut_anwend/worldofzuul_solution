package de.szut.zuul.commands;

public class Quit implements Command {
    private final String COMMANDWORD = "quit";

    @Override
    public void execute(String word) {
        System.out.println("Thank you for playing.  Good bye.");
        System.exit(0);
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }
}
