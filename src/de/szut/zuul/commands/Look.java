package de.szut.zuul.commands;

import de.szut.zuul.model.Player;

public class Look implements Command{
    private final String COMMANDWORD = "look";
    private Player player;

    public Look(Player player){
        this.player = player;
    }

    @Override
    public void execute(String word) {
        System.out.println(this.player.getCurrentRoom().getLongDescription());
    }

    @Override
    public String getCommandWord() {
        return COMMANDWORD;
    }
}
