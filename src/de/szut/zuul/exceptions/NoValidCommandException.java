package de.szut.zuul.exceptions;

public class NoValidCommandException extends Exception{
    public NoValidCommandException(String message){
        super(message);
    }
}
