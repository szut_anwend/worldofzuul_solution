package de.szut.zuul.states;

import de.szut.zuul.model.Player;

public class Immobile implements State {
    private static State instance;

    private Immobile() {

    }

    public static State getInstance(){
        if(instance == null)
            instance = new Immobile();
        return instance;
    }

    @Override
    public State heal() {
        return Wounded.getInstance();
    }

    @Override
    public State injureEasily() {
        return this;
    }

    @Override
    public State injureHeavy() {
        return this;
    }

    public String toString(){
        return "immobile";
    }
}
