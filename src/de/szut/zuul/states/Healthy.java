package de.szut.zuul.states;

import de.szut.zuul.model.Player;

public class Healthy implements State{

    private static State instance;

    private Healthy() {

    }

    public static State getInstance(){
        if(instance == null)
            instance = new Healthy();
        return instance;
    }

    @Override
    public State heal() {
        return this;
    }

    @Override
    public State injureEasily() {
        return Wounded.getInstance();
    }

    @Override
    public State injureHeavy() {
        return Immobile.getInstance();
    }

    public String toString(){
        return "healthy";
    }
}
