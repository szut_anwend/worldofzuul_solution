package de.szut.zuul.states;

import de.szut.zuul.model.Player;

public class Wounded implements State {
    private static State instance;

    private Wounded() {

    }

    public static State getInstance(){
        if(instance == null)
            instance = new Wounded();
        return instance;
    }

    @Override
    public State heal() {
        return Healthy.getInstance();
    }

    @Override
    public State injureEasily() {
        return Immobile.getInstance();
    }

    @Override
    public State injureHeavy() {
        return Immobile.getInstance();
    }

    public String toString(){
        return "wounded";
    }
}
