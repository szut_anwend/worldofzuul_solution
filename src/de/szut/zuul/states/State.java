package de.szut.zuul.states;

public interface State {
    State heal();
    State injureEasily();
    State injureHeavy();
}
